const express = require('express');
const router = express.Router();

const { getNotes, addNote, getNoteById, updateNoteById, switchNoteById, deleteNoteById } = require('../controllers/noteController');

const authMiddleware = require('../middlewares/authMiddleware');

router.get('/notes', authMiddleware, getNotes);
router.get('/notes/:id', authMiddleware, getNoteById);
router.put('/notes/:id', authMiddleware, updateNoteById);
router.patch('/notes/:id', authMiddleware, switchNoteById);
router.delete('/notes/:id', authMiddleware, deleteNoteById);
router.post('/notes', authMiddleware, addNote);

module.exports = router;