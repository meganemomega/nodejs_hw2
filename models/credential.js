const mongoose = require("mongoose");

module.exports = mongoose.model("credential",{
    username: {
        required: true,
        type: String,
        unique: true,
    },
    password: {
        required: true,
        type: String,
    }
});
