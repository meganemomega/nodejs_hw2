const Note = require('../models/note');
const CustomError = require('../models/error');
const note = require('../models/note');

module.exports.getNotes = (request, response) => {
    Note.find({userId: request.user._id}, function(err, notesList) {
        if (!notesList) {
            return response.status(400).json(new CustomError({message: 'No notes of this user found'}));  //question with repeat handler
        }
        response.status(200).json({notes: notesList});
    })
    .catch(err => {
        response.status(500).json(new CustomError({message: err.message}));
    });
};

module.exports.getNoteById = (request, response) => {
    const id = request.params.id;

    Note.findOne({_id: id, userId: request.user._id}).exec()
    .then(noteById => {
        if (!noteById) {
            return response.status(400).json(new CustomError({message: 'No note with such Id found'}));
        }
        response.status(200).json({note: noteById});
    })
    .catch(err => {
        response.status(500).json(new CustomError({message: err.message}));
    });
};

module.exports.updateNoteById = (request, response) => {
    const id = request.params.id;
    const {text} = request.body;

    Note.findOne({_id: id, userId: request.user._id}).exec()
    .then(noteById => {
        if (!noteById) {
            return response.status(400).json(new CustomError({message: 'No note with such Id found'}));
        }
        updateNote(id, request.user._id, response, {
            text: text
        });
    })
    .catch(err => {
        response.status(500).json(new CustomError({message: err.message}));
    });
};

module.exports.switchNoteById = (request, response) => {
    const id = request.params.id;

    Note.findOne({_id: id, userId: request.user._id}).exec()
    .then(noteById => {
        if (!noteById) {
            return response.status(400).json(new CustomError({message: 'No note with such Id found'}));
        }
        updateNote(id, request.user._id, response, {
            completed: !noteById.completed
        });
    })
    .catch(err => {
        response.status(500).json(new CustomError({message: err.message}));
    });
};

function updateNote(noteId, userId, response, transform) {
    Note.findOneAndUpdate({_id: noteId, userId: userId}, transform, err => {
        if (err) {
            response.status(500).json(new CustomError({message: err.message}));
        }
        response.status(200).json({message: "Success"});
    });
}

module.exports.deleteNoteById = (request, response) => {
    const id = request.params.id;

    Note.findOne({_id: id, userId: request.user._id}).exec()
    .then(noteById => {
        if (!noteById) {
            return response.status(400).json(new CustomError({message: 'No note with such Id found'}));
        }
        Note.deleteOne({_id: id, userId: request.user._id}, function (err) {
            if (err) {
                response.status(500).json(new CustomError({message: err.message}));
            }
            response.status(200).json({message: "Success"});
        });
    });
};

module.exports.addNote = (request, response) => {
    const {text} = request.body;
    if (!text) {
        return response.status(400).json(new CustomError({message: 'No text passed'}));
    }
    const userId = request.user._id;
    const completed = false;
    const createdDate = new Date();

    const note = new Note({userId, completed, text, createdDate});
    note.save()
        .then(() => {
            response.status(200).json({message: 'Success'});
        })
        .catch(err => {
            response.status(500).json(new CustomError({message: err.message}));
        });
};