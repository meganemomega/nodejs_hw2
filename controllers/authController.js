const jwt = require('jsonwebtoken');
const User = require('../models/user');
const Credential = require('../models/credential');

const { secret } = require('../config/auth');
const CustomError = require('../models/error');

module.exports.register = (request, response) => {
    const { username, password } = request.body;
    const createdDate = new Date();

    const credential = new Credential({username, password});
    credential.save()
        .then(() => {
            const user = new User({_id: credential._id, username, createdDate});
            user.save()
                .then(() => {
                    response.status(200).json({message: 'Success'});
                })
                .catch(err => {
                    response.status(500).json(new CustomError({message: err.message}));
                });
        })
        .catch(err => {
            response.status(500).json(new CustomError({message: err.message}));
        });
};

module.exports.login = (request, response) => {
    const { username, password } = request.body;

    Credential.findOne({username, password}).exec()
        .then(user => {
            if (!user ) {
                return response.status(400).json(new CustomError({message: 'No user with such username and password found'}));
            }
            response.status(200).json({token: jwt.sign(JSON.stringify(user), secret)});
        })
        .catch(err => {
            response.status(500).json(new CustomError({message: err.message}));
        });
};


